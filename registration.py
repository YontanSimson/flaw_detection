import numpy as np
import cv2 as cv
from skimage.transform import warp
from skimage.registration import optical_flow_tvl1, optical_flow_ilk
from skimage import data, draw
from skimage.registration import phase_cross_correlation
from scipy import ndimage as ndi
from skimage.filters import gaussian
import matplotlib.pyplot as plt
from skimage.restoration import (denoise_tv_chambolle, denoise_bilateral,
                                 denoise_wavelet, estimate_sigma)
from skimage import img_as_float
from sklearn.linear_model import HuberRegressor, RANSACRegressor


def calculate_shift_xy(prev: np.ndarray, next: np.ndarray, debug: bool = False) -> np.ndarray:
    """

    :param prev:
    :param next:
    :param debug:
    :return: 1x2 numpy array. XY shift in pixels
    """
    lk_params = dict(winSize=(15, 15),
                     maxLevel=5,
                     criteria=(cv.TERM_CRITERIA_EPS | cv.TERM_CRITERIA_COUNT, 10, 0.03))

    feature_params = dict(maxCorners=100,
                          qualityLevel=0.1,
                          minDistance=15,
                          blockSize=15)

    p0 = cv.goodFeaturesToTrack(prev, mask=None, **feature_params)

    if debug:
        plt.figure()
        plt.imshow(prev)
        pnts = p0.squeeze()
        plt.scatter(pnts[:, 0], pnts[:, 1])

    # KLT tracking
    p1, st1, err1 = cv.calcOpticalFlowPyrLK(prev, next, p0, None, **lk_params)  # forward
    p0r, st0, err0 = cv.calcOpticalFlowPyrLK(next, prev, p1, None, **lk_params)  # backward

    d = abs(p0 - p0r).reshape(-1, 2).max(-1)
    good = d < 1

    if debug:
        plt.figure()
        plt.imshow(next)
        pnts = p1.squeeze()
        plt.scatter(pnts[good, 0], pnts[good, 1])

    shift = (p1 - p0)[good].mean(axis=0)[0]

    return shift


def scikit_phase_corr(prev: np.ndarray, next: np.ndarray, debug: bool = False) -> np.ndarray:
    """
    Find xy registration between prev and next images using phase correlation
    :param prev: Reference image
    :param next: Inspected image
    :param debug: Show debug plots
    :return: shift_xy
    """

    shift, error, diffphase = phase_cross_correlation(prev, next,
                                             upsample_factor=16,
                                             overlap_ratio=0.5)
    v, u = -shift

    if debug:
        # --- Use the estimated optical flow for registration
        nr, nc = prev.shape

        row_coords, col_coords = np.meshgrid(np.arange(nr), np.arange(nc),
                                             indexing='ij')

        next_smooth = denoise_bilateral(next, sigma_color=0.15, sigma_spatial=15,
                                        multichannel=False)
        prev_smooth = denoise_bilateral(prev, sigma_color=0.15, sigma_spatial=15,
                                        multichannel=False)

        next_warp = warp(next_smooth, np.array([row_coords + v, col_coords + u]), order=1, preserve_range=True)

        next_warp_smooth = next_warp
        # prev_smooth = gaussian(prev, sigma=1, preserve_range=True)
        # next_warp_smooth = gaussian(next_warp, sigma=1, preserve_range=True)

        fig, axes = plt.subplots(2, 2, sharex=True, sharey=True,
                                                 figsize=(8, 8), squeeze=True)
        ax1, ax2, ax3, ax4 = axes.ravel()
        ax1.imshow(prev_smooth, cmap='gray')
        ax1.set_axis_off()
        ax1.set_title('Reference image')

        ax2.imshow(next_smooth, cmap='gray')
        ax2.set_axis_off()
        ax2.set_title('Inspected, offset image')

        ax3.imshow(next_warp_smooth, cmap='gray')
        ax3.set_axis_off()
        ax3.set_title('Warped inspected')

        mask = np.ones_like(prev) * 255
        mask_shifted = warp(mask, np.array([row_coords + v, col_coords + u]), order=0, preserve_range=True).astype(np.uint8)

        diff_image = np.abs(next_warp_smooth - prev_smooth)
        diff_image[mask_shifted == 0] = 0

        ax4.imshow(diff_image, cmap='magma')
        ax4.set_axis_off()
        ax4.set_title('Diff image')

        # Erode mask to handle border cases
        erosion_shape = cv.MORPH_ELLIPSE
        erosion_size = 1
        element = cv.getStructuringElement(erosion_shape,
                                           (2 * erosion_size + 1, 2 * erosion_size + 1),
                                           (erosion_size, erosion_size))

        mask_shifted = cv.erode(mask_shifted, element)

        X = prev_smooth[mask_shifted == 255].ravel()
        y = next_warp_smooth[mask_shifted == 255].ravel()
        plt.figure()
        plt.scatter(X, y, label="Samples")
        plt.ylabel("Next warped")
        plt.xlabel("Prev")

        from sklearn.linear_model import HuberRegressor, RANSACRegressor
        from sklearn import linear_model

        line_X = np.arange(X.min(), X.max(), (X.max() - X.min()) / 255.)[:, np.newaxis]
        ransac = linear_model.RANSACRegressor()
        ransac.fit(X[:, np.newaxis], y)
        inlier_mask = ransac.inlier_mask_
        outlier_mask = np.logical_not(inlier_mask)

        # Predict data of estimated models
        line_y_ransac = ransac.predict(line_X)

        huber = HuberRegressor(alpha=0.0, epsilon=1.35)
        huber.fit(X[:, np.newaxis], y)
        line_y_huber = huber.coef_ * line_X + huber.intercept_

        plt.scatter(line_X, line_y_ransac, label="RANSAC")
        plt.scatter(line_X, line_y_huber, label="Huber")
        plt.legend()

        diff_image = np.abs(huber.coef_ * next_warp + huber.intercept_ - prev_smooth)
        diff_image[mask_shifted == 0] = 0
        plt.figure()
        plt.imshow(diff_image * 255)
        plt.title("Diff image")
        plt.show()

    return -shift[::-1]


def scikit_diff_image(prev, next, shift: np.ndarray, debug: bool = True) -> np.ndarray:
    """
    Calculate warped next image and subtract it from prev image.
    Account for difference in exposure with robust linear regression
    :param prev:
    :param next:
    :param shift:
    :param debug:
    :return: diff image
    """
    u, v = shift
    nr, nc = prev.shape

    row_coords, col_coords = np.meshgrid(np.arange(nr), np.arange(nc),
                                         indexing='ij')

    prev_smooth = gaussian(img_as_float(prev), sigma=1, preserve_range=True)
    next_smooth = gaussian(img_as_float(next), sigma=1, preserve_range=True)

    next_warp = warp(next_smooth, np.array([row_coords + v, col_coords + u]), order=1, preserve_range=True)

    next_warp_smooth = next_warp

    mask = np.ones_like(prev) * 255
    mask_shifted = warp(mask, np.array([row_coords + v, col_coords + u]), order=0, preserve_range=True).astype(np.uint8)

    diff_image = np.abs(next_warp_smooth - prev_smooth)
    diff_image[mask_shifted == 0] = 0

    if debug:
        fig, axes = plt.subplots(2, 2, sharex=True, sharey=True,
                                 figsize=(8, 8), squeeze=True)
        ax1, ax2, ax3, ax4 = axes.ravel()
        ax1.imshow(prev_smooth, cmap='gray')
        ax1.set_axis_off()
        ax1.set_title('Reference image')

        ax2.imshow(next_smooth, cmap='gray')
        ax2.set_axis_off()
        ax2.set_title('Inspected, offset image')

        ax3.imshow(next_warp_smooth, cmap='gray')
        ax3.set_axis_off()
        ax3.set_title('Warped inspected')

        ax4.imshow(diff_image, cmap='magma')
        ax4.set_axis_off()
        ax4.set_title('Diff image')

    # Erode mask to handle border cases
    erosion_shape = cv.MORPH_ELLIPSE
    erosion_size = 1
    element = cv.getStructuringElement(erosion_shape,
                                       (2 * erosion_size + 1, 2 * erosion_size + 1),
                                       (erosion_size, erosion_size))

    mask_shifted = cv.erode(mask_shifted, element)

    X = prev_smooth[mask_shifted == 255].ravel()
    y = next_warp_smooth[mask_shifted == 255].ravel()

    line_X = np.arange(X.min(), X.max(), (X.max() - X.min()) / 255.)[:, np.newaxis]
    ransac = RANSACRegressor()
    ransac.fit(X[:, np.newaxis], y)
    inlier_mask = ransac.inlier_mask_
    outlier_mask = np.logical_not(inlier_mask)

    # Predict data of estimated models
    line_y_ransac = ransac.predict(line_X)

    huber = HuberRegressor(alpha=0.0, epsilon=1.35)
    huber.fit(X[:, np.newaxis], y)
    line_y_huber = huber.coef_ * line_X + huber.intercept_

    if debug:
        plt.figure()
        plt.scatter(X, y, label="Samples")
        plt.ylabel("Next warped")
        plt.xlabel("Prev")
        plt.scatter(line_X, line_y_ransac, label="RANSAC")
        plt.scatter(line_X, line_y_huber, label="Huber")
        plt.legend()

    coeff = huber.coef_
    bias = huber.intercept_
    diff_image = np.abs(coeff * next_warp + bias - prev_smooth)
    diff_image[mask_shifted == 0] = 0

    if debug:
        plt.figure()
        plt.imshow(diff_image * 255)
        plt.title("Diff image")

    return diff_image * 255, (next_warp * 255).astype(np.uint8)


def scikit_registration(prev: np.ndarray, next: np.ndarray) -> np.ndarray:
    """
    Refine stage to account for minor differences between two examples of the same chip
    :param prev:
    :param next:
    :return:
    """
    # --- Compute the optical flow
    v, u = optical_flow_ilk(prev, next, radius=3, num_warp=3)

    # --- Compute flow magnitude
    norm = np.sqrt(u ** 2 + v ** 2)

    # --- Display
    fig, (ax0, ax1) = plt.subplots(1, 2, figsize=(8, 4))

    # --- Sequence image sample

    ax0.imshow(prev, cmap='gray')
    ax0.set_title("Sequence image sample")
    ax0.set_axis_off()

    # --- Quiver plot arguments

    nvec = 20  # Number of vectors to be displayed along each image dimension
    nl, nc = prev.shape
    step = max(nl//nvec, nc//nvec)

    y, x = np.mgrid[:nl:step, :nc:step]
    u_ = u[::step, ::step]
    v_ = v[::step, ::step]

    ax1.imshow(norm)
    ax1.quiver(x, y, u_, v_, color='r', units='dots',
               angles='xy', scale_units='xy', lw=3)
    ax1.set_title("Optical flow magnitude and vector field")
    ax1.set_axis_off()
    fig.tight_layout()

    plt.show()

    return np.array([u, v])


def shift_image(
        img: np.ndarray,
        shift_xy: np.ndarray,
        interpolation_type: int = cv.INTER_LINEAR,
) -> np.ndarray:
    """
    Shift image by shift_xy

    :param img:
    :param shift_xy:
    :param interpolation_type:
    :return: Shifted image
    """
    h, w = img.shape
    indy, indx = np.indices((h, w), dtype=np.float32)

    map_x = indx + shift_xy[0]
    map_y = indy + shift_xy[1]

    img_shifted = cv.remap(img, map_x, map_y, interpolation_type)

    return img_shifted



