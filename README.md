# README #

The purpose of this code is to find defects in silicon wafers. 
### Silicon Flaw Detection ###

* Given a pair of images, the reference with no flaws and inspection image show the 
* Test with Python 3.6, Ubuntu 18.04 LTS

### Setup and Run ###

#### Setup ####

```
# Clone the repo from bitbucket
git clone git@bitbucket.org:YontanSimson/flaw_detection.git

# Create virtual nvironment
cd flaw_detection
python3 -m venv venv
source venv/bin/active
pip install -r requirments
```

#### Run example ####
```
python main.py ./data /home/$USER/flaw_detection_results
```


