import numpy as np
import cv2 as cv
import matplotlib.pyplot as plt
from typing import Tuple
from skimage.feature import blob_log, blob_doh, blob_dog


def blob_detection(diff_image: np.ndarray,
                   max_sigma: float = 20,
                   num_sigma: int = 10,
                   threshold: float = 20.,
                   debug: bool = True) -> Tuple[np.ndarray, np.ndarray]:
    """
    Detect blobs in diff image
    :param diff_image:
    :param max_sigma:
    :param num_sigma:
    :param threshold:
    :param debug:
    :return: (blobs, masked image)
    """
    blobs_log = blob_log(diff_image, max_sigma=max_sigma, num_sigma=num_sigma, threshold=threshold)

    if debug:
        blobs_doh = blob_doh(diff_image, max_sigma=max_sigma, num_sigma=num_sigma, threshold=threshold)
        blobs_dog = blob_dog(diff_image, max_sigma=max_sigma, overlap=.01, threshold=threshold)
        fig, axes = plt.subplots(1, 3, sharey=True, sharex=True)
        ax1, ax2, ax3 = axes
        ax1.imshow(diff_image)
        for blob in blobs_dog:
            y, x, r = blob
            c = plt.Circle((x, y), r, color='magenta', linewidth=2, fill=False)
            ax1.add_patch(c)
        ax1.set_axis_off()
        ax1.set_title("DoG")

        ax2.imshow(diff_image)
        # colors = ['yellow', 'lime', 'red']
        for blob in blobs_doh:
            y, x, r = blob
            c = plt.Circle((x, y), r, color='lime', linewidth=2, fill=False)
            ax2.add_patch(c)
        ax2.set_axis_off()

        ax2.set_title("DoH")

        ax3.imshow(diff_image)
        for blob in blobs_log:
            y, x, r = blob
            c = plt.Circle((x, y), r, color='red', linewidth=2, fill=False)
            ax3.add_patch(c)
        ax3.set_axis_off()
        ax3.set_title("LoG")

    # Create masked image
    blobs_mask = np.zeros(diff_image.shape, dtype=np.uint8)
    for blob in blobs_log:
        y, x, r = blob
        mask_circle = np.zeros(diff_image.shape, dtype=np.uint8)
        mask_circle = cv.circle(mask_circle, (int(x), int(y)), int(r * 1.2), (255, ), thickness=-1, lineType=cv.LINE_8)

        # Get local threshold
        blob_values = diff_image[mask_circle == 255]
        th_blob = np.percentile(blob_values, q=10)

        blob_mask = (diff_image > th_blob) & mask_circle
        blobs_mask |= blob_mask

    return blobs_log, blobs_mask
