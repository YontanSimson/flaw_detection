import os
import numpy as np
import cv2 as cv
import argparse
from typing import Dict, Tuple, List
import re
import matplotlib.pyplot as plt

from registration import (
    calculate_shift_xy,
    shift_image,
    scikit_diff_image,
    scikit_phase_corr,
)

from flaw_detection import blob_detection


def parse_dir(input_dir: str, defective: bool) -> Dict:
    """

    :param input_dir: Directory containing pairs of images with reference and inspected examples
    :param defective: Marks the cases as defective or not
    :return:
    """

    image_fn_list = os.listdir(input_dir)

    case_list = {}

    for fn in image_fn_list:
        if not fn.endswith(".tif"):
            continue
        image_path = os.path.join(input_dir, fn)
        m = re.search("^(case\d+)_(reference|inspected)_image.tif", fn)
        res = m.groups()

        case_id, ref_inspec = res
        if case_id in case_list:
            case_list[case_id].append((image_path, ref_inspec, defective))
        else:
            case_list[case_id] = [(image_path, ref_inspec, defective)]

    return case_list


def read_image_pair(img_pair: List[Tuple[str, str, bool]]) -> Tuple[np.ndarray, np.ndarray]:
    """
    Read inspected and reference image pair from disk
    :param img_pair:
    :return:
    """
    ref_img = None
    inspected_img = None
    for sample in img_pair:
        image_path, ref_inspec, defective = sample
        if ref_inspec == 'reference':
            ref_img = cv.imread(image_path, 0)
        else:  # ref_inspec == 'inspected'
            inspected_img = cv.imread(image_path, 0)

    return ref_img, inspected_img, defective


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("data_dir", help="Folder namefor the data", type=str)
    parser.add_argument("output_dir", help="Folder for output results", type=str)
    parser.add_argument("-th", "--threshold", help="threshold for Blob detector", default=20.)
    parser.add_argument("-d", "--debug", help="Show debug plots", action="store_true")
    args = parser.parse_args()

    data_dir = args.data_dir
    output_dir = args.output_dir
    threshold = args.threshold
    debug = args.debug

    defective_examples_dir = os.path.join(data_dir, "defective_examples")
    non_defective_examples_dir = os.path.join(data_dir, "non_defective_examples")
    gt_path = os.path.join(data_dir, "defective_examples", "defects locations.txt")

    defective_cases = parse_dir(defective_examples_dir, True)
    non_defective_cases = parse_dir(non_defective_examples_dir, False)

    case_list = {**defective_cases, **non_defective_cases}

    for case_id, img_pair in case_list.items():

        # Read reference and inspected pair
        ref_img, inspected_img, defective = read_image_pair(img_pair)

        fig, axes = plt.subplots(1, 2, sharex=True, sharey=True)
        axes[0].imshow(ref_img)
        axes[0].set_title("Ref")
        axes[0].axis('off')
        axes[1].imshow(inspected_img)
        axes[1].set_title("Inspected")
        axes[1].axis('off')
        fig.savefig(os.path.join(output_dir, f"{case_id}_input.png"))

        # Compare two samples
        next = inspected_img
        prev = ref_img
        shift_ = calculate_shift_xy(prev, next)
        shift = scikit_phase_corr(prev, next, debug=debug)

        diff_image, warped_next = scikit_diff_image(prev, next, shift, debug=debug)

        # Blob detection
        blobs, blob_mask = blob_detection(diff_image, threshold=threshold, debug=debug)

        blob_mask_shifted = shift_image(blob_mask, -shift, interpolation_type=cv.INTER_NEAREST)
        fig, axes = plt.subplots(1, 2, sharex=True, sharey=True)
        ax1, ax2 = axes
        ax1.imshow(blob_mask_shifted)
        ax1.set_title("Flaw mask")
        ax1.axis('off')
        ax2.imshow(next)
        ax2.set_title("Inspected")
        ax2.axis('off')

        if not os.path.isdir(output_dir):
            os.makedirs(output_dir)
        fig.savefig(os.path.join(output_dir, f"{case_id}_output.png"))

    plt.show()
    print("Done")
